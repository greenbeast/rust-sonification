// :DONE: Setup fundsp with the snare and base strings to get an audio output
// :TODO: Setup multithreading because this is a bit resource heavy

#[allow(unused_must_use)]
#[allow(clippy::precedence)]
#[allow(dead_code)]
use std::error::Error;
use std::io;
use std::process;

use fundsp::hacker::*;

use serde::Deserialize;

// By default, struct field names are deserialized based on the position of
// a corresponding field in the CSV data's header record.
#[derive(Debug, Deserialize)]
struct Record {
    population: u64,
}

struct Data {
    val: Vec<i64>,
    smallest: Vec<i64>,
    smaller: Vec<i64>,
    bigger: Vec<i64>,
    biggest: Vec<i64>,
}

impl Data {
    fn example() -> Result<(), Box<dyn Error>> {
        let mut rdr = csv::Reader::from_reader(io::stdin());
	let mut val = Vec::new();
        for result in rdr.deserialize() {
            // Notice that we need to provide a type hint for automatic
            // deserialization.
            let record: Record = result?;
	    val.push(record.population);
    	    // Set these values to a list then play sound based on overall value?
        }

	// Need to get the min and max values here
	// All needs to be moved to after normalization
	let val_min = val.iter().min().unwrap();
	let val_max = val.iter().max().unwrap();

	// Initializing vectors
	let mut smallest = Vec::new();
	let mut smaller = Vec::new();
	let mut biggest = Vec::new();
	let mut bigger = Vec::new();

	// Maybe I can normalize the array so I don't have such huge gaps
	let mut normalized = Vec::new();
	for i in 0..val.len() {
	    let val_float = val[i] as f64;
	    let val_min = *val_min as f64;
	    let val_max = *val_max as f64;
	    let val_normalized: f64 = (val_float - val_min)/(val_max-val_min) as f64;
	    normalized.push(val_normalized);
	}
	
	// Levels to this shit
	// Normalized min and max are 0 and 1 respectively, should hardcode? Same with mid?
	let normalized_min = normalized[1];
	let normalized_max = normalized[normalized.len()-2];
	let normalized_mid = (normalized_max + normalized_min)/2.0;
	let normalized_lower = (normalized_mid + normalized_min)/2.0;
	let normalized_upper = (normalized_mid + normalized_max)/2.0;

	// :TODO: What I need is a list that explains the order os these, rather than seperate lists
	// Match statement here ??????
	let mut order = Vec::new();
	for i in 0..normalized.len() {
	    if normalized[i] <= normalized_lower {
		smallest.push(normalized[i]);
		order.push("smallest");
	    } else if normalized[i] <= normalized_mid {
		smaller.push(normalized[i]);
		order.push("smaller");
	    } else if normalized[i] <= normalized_upper {
		bigger.push(normalized[i]);
		order.push("bigger");
	    } else if normalized[i] == 0.0 {
		smallest.push(normalized[i]);
		order.push("smallest");
	    } else {
		biggest.push(normalized[i]);
		order.push("biggest");
	    }
	    
	}

	let mut base_line = String::from("");
	// rename to snare_line
	let mut snard_line = String::from("");

	// so this is cycling through out list and adding to our base and snare lines which is
	// then processed later and turned into audio. Basically just taking the empty strings
	// from above and appending to them with push_str. Each x is a beat and each . is a skip
	// for some reason it won't let me push empty strings or just .'s
	for i in 0..order.len() {
	    if order[i] == "smallest" {
		base_line.push_str("...x");
		snard_line.push_str("...x");
	    } else if order[i] == "smaller" {
		base_line.push_str(".x..");
		snard_line.push_str(".x..");
	    } else if order[i] == "bigger" {
		base_line.push_str(".x..x.");
		snard_line.push_str(".x..x.");
	    } else if order[i] == "biggest" {
		base_line.push_str(".xxx");
		snard_line.push_str(".xxx");
	    }
	}
	// Calls the function and passes the written strings for the base and snare lines
	combine(&base_line.to_string(), &snard_line.to_string());
	// prints the base and snare line just to see the output
	println!("baseline:\n{}\n\nsnareline:\n{}", base_line, snard_line);
	
	Ok(())
    }
}


fn combine(baseline: &str, snareline: &str) {
    let sample_rate = 22100.0;
    // What I can do is make these strings an argument and add to them so for things like
    // a bigger wait you just add more ... and bigger items use xxx
    let bassd_line = baseline;
    let snare_line = snareline;

    let bassdrum =
        || envelope(|t| 200.0 * exp(-t * 5.0)) >> sine() >> shape(Shape::Tanh(2.0)) >> pan(0.0);

    let env = || envelope(|t| exp(-t * 10.0));
    let snaredrum = || pink() * env() | pink() * env();

    let mut sequencer = Sequencer::new(sample_rate, 2);

    let length = bassd_line.as_bytes().len();
    // Play with bpm to see how it feels
    let bpm = 128.0 * 4.0;
    // length of the audio clip in seconds, the adder on the end is just to make the ending less abrupt
    let duration = length as f64 / bpm_hz(bpm);// + 0.25;

    // Need tp figure out how this really works
    for i in 0..length {
        let t0 = i as f64 / bpm_hz(bpm);
        let t1 = (i + 6) as f64 / bpm_hz(bpm);
	// Reads the string byte by byte and when an x comes up, do a basedrum
        if bassd_line.as_bytes()[i] == b'x' {
            sequencer.add64(t0, t1, 0.0, 0.4, Box::new(bassdrum()));
        }
	// Reads the string byte by byte and when an x comes up, do a snaredrum
        if snare_line.as_bytes()[i] == b'x' {
            sequencer.add64(t0, t1, 0.0, 0.1, Box::new(snaredrum()));
        }
    }

    let wave = Wave64::render(sample_rate, duration, &mut sequencer);

    let wave = wave.filter(duration, &mut (reverb_stereo(0.1, 2.0) * 3.0));

    let wave = wave.filter_latency(duration, &mut (limiter_stereo((0.05, 0.20))));

    wave.save_wav16(std::path::Path::new("sequence.wav"));

}


fn main() {
    if let Err(err) = Data::example() {
        println!("error running example: {}", err);
        process::exit(1);
    }
    println!("You audio has been sonified! It is in sequences.wav in this directory.");

}
